module gitlab.com/gitlab-org/security-products/analyzers/semgrep

go 1.15

require (
	github.com/Microsoft/go-winio v0.5.2 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20220812175011-7fcef0dbe794 // indirect
	github.com/cloudflare/circl v1.2.0 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.0
	github.com/urfave/cli/v2 v2.11.2
	gitlab.com/gitlab-org/security-products/analyzers/command v1.9.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.0
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.13.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
	golang.org/x/crypto v0.0.0-20220817201139-bc19a97f63c8 // indirect
	golang.org/x/net v0.0.0-20220812174116-3211cb980234 // indirect
	golang.org/x/sys v0.0.0-20220817070843-5a390386f1f2 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
